import { createStore } from 'vuex'
import axios from "axios";

export const store = createStore({
    state: {
        filtered_apartments: [],
        priceMin: '',
        priceMax: '',
        selected_filters_layout: [],
        selected_slider: []
    },
    mutations: {
        SET_APARTMENTS_TO_STATE(state, apartments) {
            state.filtered_apartments = apartments

            store.commit('FILTERS_BY_APARTMENTS')
        },
        SET_PRICE_RANGE_SLIDER(state, price) {
            state.priceMin = price[0];
            state.priceMax = price[1];
        },
        FILTERS_BY_APARTMENTS() {
            store.commit('FILTERS_BY_NUMBER_ROOMS')
            store.commit('FILTERS_BY_LAYOUT_HOUSE')
            store.commit('FILTERS_BY_PROJECT')
            store.commit('FILTERS_BY_PRICE')
        },
        FILTERS_BY_CHECKBOX(state, elem) {
            state.filtered_apartments.forEach(item => {
                let condition = function(){
                    return state.selected_filters_layout.includes(item[elem])
                }
                if (state.filtered_apartments.some(condition)) {
                    state.filtered_apartments = state.filtered_apartments.filter(function(item) {
                        return state.selected_filters_layout.includes(item[elem])
                    })
                }
            })
        },
        FILTERS_BY_NUMBER_ROOMS() {
            store.commit('FILTERS_BY_CHECKBOX', 'numberOfRooms')
        },
        FILTERS_BY_LAYOUT_HOUSE() {
            store.commit('FILTERS_BY_CHECKBOX', 'peculiarities')
        },
        FILTERS_BY_PROJECT() {
            store.commit('FILTERS_BY_CHECKBOX', 'housingComplexNameFilter')
        },
        FILTERS_BY_PRICE(state) {
            state.filtered_apartments = state.filtered_apartments.filter(function(item) {
                return (item.price >= +state.priceMin) && (item.price <= +state.priceMax)
            })
        },
    },
    actions: {
        GET_APARTMENTS_FROM_API({commit}) {
            /// чтобы запустить json-serve нужно заменить ссылку на http://localhost:3000/apartments и запустить сам сервер командой json-server --watch db.json
            /// обход CORS https://cors-anywhere.herokuapp.com/https://grishov.ru/db.json
            return axios('http://localhost:3000/apartments', {
                method: "GET"
            })
                .then((response) => {
                    commit('SET_APARTMENTS_TO_STATE', response.data)

                    return response;
                })
                .catch((error) => {

                    return error;
                })
        },
    }
});
