import { createApp } from 'vue'
import App from './App.vue'
import { store } from './vuex/store'
import '@/assets/styles/style.scss'


let app = createApp(App)

app.use(store)
app.mount('#app')
